# -*- coding: utf-8 -*-
"""
/***************************************************************************
 rg_biomass
                                 A QGIS plugin
 Assess and identify suitable areas for biomass energy plants
                              -------------------
        begin                : 2014-12-16
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Francesco Geri
        email                : fgeri@icloud.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from rg_biomass_dialog import rg_biomassDialog
import os.path


import os
import sys
import subprocess
import shutil
import binascii
import tempfile

from multiprocessing import Process, Queue

import time

import string

from osgeo import gdal,ogr
  



###########

grass7path = r'C:\OSGeo4W\apps\grass\grass-7.1.svn'
grass7bin_win = r'C:\OSGeo4W\bin\grass71svn.bat'
# Linux
grass7bin_lin = 'grass71'
# MacOSX
grass7bin_mac = '/Applications/GRASS/GRASS-7.1.app/'
#myepsg = '4326' # latlong
myepsg = '32632' # ETRS-TM32, http://spatialreference.org/ref/epsg/3044/
#myfile = '/home/neteler/markus_repo/books/kluwerbook/data3rd/lidar/lidar_raleigh_nc_spm.shp'
#myfile = '/data/maps/world_natural_earth_250m/europe_north_east.tif'
#myfile = '/home/f/Documents/triglav/ecological.asc'
#myfile = r'C:\Dati\Padergnone\square_p95.tif'



########### SOFTWARE
if sys.platform.startswith('linux'):
    # we assume that the GRASS GIS start script is available and in the PATH
    # query GRASS 7 itself for its GISBASE
    grass7bin = grass7bin_lin
elif sys.platform.startswith('win'):
    grass7bin = grass7bin_win
else:
    OSError('Platform not configured.')
 
startcmd = grass7bin + ' --config path'
 
p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, "ERROR: Cannot find GRASS GIS 7 start script (%s)" % startcmd
    sys.exit(-1)
if sys.platform.startswith('linux'):
    gisbase = out.strip('\n')
elif sys.platform.startswith('win'):
    if out.find("OSGEO4W home is") != -1:
        gisbase = out.strip().split('\n')[1]
    else:
        gisbase = out.strip('\n')
    os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin', 'sh.exe')
 
# Set GISBASE environment variable
os.environ['GISBASE'] = gisbase
# define GRASS-Python environment
gpydir = os.path.join(gisbase, "etc", "python")
sys.path.append(gpydir)
########
# define GRASS DATABASE

"""
if sys.platform.startswith('win'):
    gisdb = os.path.join(os.getenv('APPDATA', 'grassdata')
else:
    gisdb = os.path.join(os.getenv('HOME', 'grassdata')
""" 
# override for now with TEMP dir
gisdb = os.path.join(tempfile.gettempdir(), 'grassdata')
try:
    os.stat(gisdb)
except:
    os.mkdir(gisdb)
 
# location/mapset: use random names for batch jobs
string_length = 16
location = binascii.hexlify(os.urandom(string_length))
mapset   = 'PERMANENT'
location_path = os.path.join(gisdb, location)
 
# Create new location (we assume that grass7bin is in the PATH)
#  from EPSG code:
startcmd = grass7bin + ' -c epsg:' + myepsg + ' -e ' + location_path
#  from SHAPE or GeoTIFF file
#startcmd = grass7bin + ' -c ' + myfile + ' -e ' + location_path
 
#print startcmd
p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, 'ERROR: Cannot generate location (%s)' % startcmd
    sys.exit(-1)
#else:
    #print 'Created location %s' % location_path
 
# Now the location with PERMANENT mapset exists.
 
########
# Now we can use PyGRASS or GRASS Scripting library etc. after 
# having started the session with gsetup.init() etc
 
# Set GISDBASE environment variable
os.environ['GISDBASE'] = gisdb
 
# Linux: Set path to GRASS libs (TODO: NEEDED?)
path = os.getenv('LD_LIBRARY_PATH')
dir  = os.path.join(gisbase, 'lib')
if path:
    path = dir + os.pathsep + path
else:
    path = dir
os.environ['LD_LIBRARY_PATH'] = path
 
# language
os.environ['LANG'] = 'en_US'
os.environ['LOCALE'] = 'C'
 
# Windows: NEEDED?
#path = os.getenv('PYTHONPATH')
#dirr = os.path.join(gisbase, 'etc', 'python')
#if path:
#    path = dirr + os.pathsep + path
#else:
#    path = dirr
#os.environ['PYTHONPATH'] = path
 
#print os.environ
 
## Import GRASS Python bindings
import grass.script as grass
import grass.script.setup as gsetup
from grass.script.core import run_command, parser, overwrite, read_command



class rg_biomass:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas=self.iface.mapCanvas()
        self.msgBar = self.iface.messageBar()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'rg_biomass_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = rg_biomassDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&R.green')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'rg_biomass')
        self.toolbar.setObjectName(u'rg_biomass')


        self.dlg.button_box_th.accepted.connect(self.theoretical)
        self.dlg.button_box_leg.accepted.connect(self.legal)
        #self.dlg.button_box_th.reset.connect(self.reset_field)
        self.dlg.button_reset_base.clicked.connect(self.reset_field)
        self.dlg.comboforest.currentIndexChanged[str].connect(self.reset_combo)
        self.dlg.button_save_folder.clicked.connect(self.choose_out_folder)
        #self.dlg.button_box_th.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.reset_field)


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('rg_biomass', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/rg_biomass/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'R.green.biomass'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&r.green.biomass'),
                action)
            self.iface.removeToolBarIcon(action)


    def choose_out_folder(self):
        fname = QFileDialog.getExistingDirectory(None,"Open a folder","/",QFileDialog.ShowDirsOnly)
        self.dlg.line_out_folder.setText(fname) 
        # if self.tipofile=="csv":
        #     self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','csv files (*.csv);;all files (*.*)')
        #     self.dlg_conf.path_to_kmean.setText(self.fname)            
        # if self.tipofile=="tif":
        #     self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','GeoTiff files (*.tif);;all files (*.*)')
        #     self.dlg_reclass.output_raster_class.setText(self.fname)            
    

    def process_theoretical(self):
        q = Queue()
        p=Process(target=self.theoretical,args=(q,))
        p.start()


    def reset_field(self):
        self.dlg.comboforest.clear()
        self.dlg.combosurface.clear()
        self.dlg.combostudy.clear()
        self.dlg.combomanag.clear()
        self.dlg.combotreat.clear()
        self.dlg.line_energy_tbhf.setText("0.49")
        self.dlg.line_energy_whf.setText("1.57")
        self.dlg.line_energy_tbc.setText("0.55")
        self.allLayers = self.canvas.layers()
        self.listalayers=dict()
        #self.elementovuoto="No required"
        # self.dlg.comboclimate.addItem(self.elementovuoto)
        # self.dlg.comboaspect.addItem(self.elementovuoto)
        for self.i in self.allLayers:
            self.listalayers[self.i.name()]=self.i
            if self.i.type() == QgsMapLayer.VectorLayer:            
                self.dlg.comboforest.addItem(str(self.i.name()))
                self.dlg.combostudy.addItem(str(self.i.name())) 
            # if self.i.type() == QgsMapLayer.RasterLayer:           
            #     self.dlg.comboclimate.addItem(str(self.i.name()))
            #     self.dlg.comboaspect.addItem(str(self.i.name()))  



    def reset_combo(self):

        self.dlg.combosurface.clear()
        self.dlg.combomanag.clear()
        self.dlg.combotreat.clear()
        self.dlg.combo_increment.clear()
        self.dlg.combo_legal.clear()

        forest_text = str(self.dlg.comboforest.currentText()) 
        #print forest_text


        if forest_text!="":

            forest_layer=self.listalayers[forest_text]

            forest_fields = forest_layer.pendingFields()
            field_names = [field.name() for field in forest_fields]


            for text in field_names:
                self.dlg.combomanag.addItem(text)
                self.dlg.combotreat.addItem(text)
                self.dlg.combosurface.addItem(text)
                self.dlg.combo_increment.addItem(text)
                self.dlg.combo_legal.addItem(text)



    def theoretical(self):
        #q.put([42, None, 'hello'])

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])

        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])


        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        increment_field = str(self.dlg.combo_increment.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix.text()

        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())


        # print parcel
        # print boundaries
        # print area_field
        # print treatment_field
        # print management_field
        # print increment_field
        # print resolution
        # print srs
        # print output_directory
        # print prefix_output
        # print energy_tops_hf
        # print energy_cormometric_vol_hf
        # print energy_tops_cop



        ###########


        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        #recovery data from gui
        # boundaries='/home/f/particellare_pnam/particellare.shp'
        # parcel='/home/f/particellare_pnam/particellare.shp'
        # increment_field='yield'
        # area_field='Hectares'
        # management_field='management'
        # treatment_field='treatment'
        # prefix_output='prova'
        # output_directory='/home/f/Desktop/'
        # resolution=5

        # energy_tops_hf=0.49
        # energy_cormometric_vol_hf=1.57
        # energy_tops_cop=0.55

        #grass map import

        run_command("v.in.ogr", dsn=parcel, output="forest",flags = 'o') 

        run_command("v.in.ogr", dsn=boundaries, output="boundaries",flags = 'o') 

        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="increment", use="attr", attrcolumn=increment_field)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=treatment_field)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field)

        #g.region vect=forest@PERMANENT res=5



        #r.green processing

        run_command("r.green.biomassfor.theoretical",energy_tops_hf=energy_tops_hf,energy_cormometric_vol_hf=energy_cormometric_vol_hf,
                    increment='increment',yield_surface="yield_surface",management="management",treatment="treatment",
                    output_prefix=prefix_output)

        maptot=prefix_output+"_p_bioenergy"
        maphf=prefix_output+"_p_bioenergyHF"
        mapc=prefix_output+"_p_bioenergyC"

        #export map

        run_command("r.out.gdal",input=maptot,output=output_directory+"/"+maptot+".asc",format="GTiff")
        run_command("r.out.gdal",input=maphf,output=output_directory+"/"+maphf+".asc",format="GTiff")
        run_command("r.out.gdal",input=mapc,output=output_directory+"/"+mapc+".asc",format="GTiff")


        #statistic=read_command("r.univar",rast=maptot)


        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 


        tempo=time.time() - start_time

        self.dlg.console_r.appendPlainText("***R.green.biomassfor.theoretical***\n\n")
        self.dlg.console_r.appendPlainText("Processing executed\n\n")
        self.dlg.console_r.appendPlainText("Process time: "+str(tempo))
        self.dlg.console_r.appendPlainText("\n\n")
        self.dlg.console_r.appendPlainText("Resulted maps:\n")
        self.dlg.console_r.appendPlainText("1) "+maptot)
        self.dlg.console_r.appendPlainText("2) "+maphf)
        self.dlg.console_r.appendPlainText("3) "+mapc)
        self.dlg.console_r.appendPlainText("\n\n")
        self.dlg.console_r.appendPlainText("Map added to the table of content\n")
        self.dlg.console_r.appendPlainText("---------------------------------\n\n")

        #self.dlg.console_r.appendPlainText(statistic)

    def legal(self):

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])

        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])


        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_legal.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix.text()

        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())


        # print parcel
        # print boundaries
        # print area_field
        # print treatment_field
        # print management_field
        # print yield_field
        # print resolution
        # print srs
        # print output_directory
        # print prefix_output
        # print energy_tops_hf
        # print energy_cormometric_vol_hf
        # print energy_tops_cop



        ###########


        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        #recovery data from gui
        # boundaries='/home/f/particellare_pnam/particellare.shp'
        # parcel='/home/f/particellare_pnam/particellare.shp'
        # yield_field='yield'
        # area_field='Hectares'
        # management_field='management'
        # treatment_field='treatment'
        # prefix_output='prova'
        # output_directory='/home/f/Desktop/'
        # resolution=5

        # energy_tops_hf=0.49
        # energy_cormometric_vol_hf=1.57
        # energy_tops_cop=0.55

        #grass map import

        run_command("v.in.ogr", dsn=parcel, output="forest",flags = 'o') 

        run_command("v.in.ogr", dsn=boundaries, output="boundaries",flags = 'o') 

        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="yield", use="attr", attrcolumn=yield_field)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=treatment_field)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field)

        #g.region vect=forest@PERMANENT res=5



        #r.green processing

        run_command("r.green.biomass.legal",energy_tops_hf=energy_tops_hf,energy_cormometric_vol_hf=energy_cormometric_vol_hf,
                    yield_='yield',yield_surface="yield_surface",management="management",treatment="treatment",
                    output_prefix=prefix_output)

        maptot=prefix_output+"_l_bioenergy"
        maphf=prefix_output+"_l_bioenergyHF"
        mapc=prefix_output+"_l_bioenergyC"

        #export map

        run_command("r.out.arc",input=maptot,output=output_directory+"/"+maptot+".asc")
        run_command("r.out.arc",input=maphf,output=output_directory+"/"+maphf+".asc")
        run_command("r.out.arc",input=mapc,output=output_directory+"/"+mapc+".asc")


        #statistic=read_command("r.univar",rast=maptot)


        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 


        tempo=time.time() - start_time

        # self.dlg.console_r.appendPlainText("***R.green.biomassfor.theoretical***\n\n")
        # self.dlg.console_r.appendPlainText("Processing executed\n\n")
        # self.dlg.console_r.appendPlainText("Process time: "+str(tempo))
        # self.dlg.console_r.appendPlainText("\n\n")
        # self.dlg.console_r.appendPlainText("Resulted maps:\n")
        # self.dlg.console_r.appendPlainText("1) "+maptot)
        # self.dlg.console_r.appendPlainText("2) "+maphf)
        # self.dlg.console_r.appendPlainText("3) "+mapc)
        # self.dlg.console_r.appendPlainText("\n\n")
        # self.dlg.console_r.appendPlainText("Map added to the table of content\n")
        # self.dlg.console_r.appendPlainText("---------------------------------\n\n")

        #self.dlg.console_r.appendPlainText(statistic)



    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        self.reset_field()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass
