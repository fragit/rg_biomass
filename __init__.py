# -*- coding: utf-8 -*-
"""
/***************************************************************************
 rg_biomass
                                 A QGIS plugin
 Assess and identify suitable areas for biomass energy plants
                             -------------------
        begin                : 2014-12-16
        copyright            : (C) 2014 by Francesco Geri
        email                : fgeri@icloud.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load rg_biomass class from file rg_biomass.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .rg_biomass import rg_biomass
    return rg_biomass(iface)
