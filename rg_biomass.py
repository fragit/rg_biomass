# -*- coding: utf-8 -*-
"""
/***************************************************************************
 rg_biomassfor
                                 A QGIS plugin
 Assess and identify suitable areas for biomass energy plants
                              -------------------
        begin                : 2014-12-16
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Francesco Geri
        email                : fgeri@icloud.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from rg_biomass_dialog import rg_biomassDialog
import os.path

import logging
from PyQt4.QtCore import QObject,pyqtSignal


import os
from os.path import expanduser
import platform
import sys
from sys import stderr, stdout
import subprocess
import commands
import shutil
import binascii
import tempfile

from multiprocessing import Process, Queue

import time

import string

from osgeo import gdal,ogr
  


###########

grass7path = r'C:\OSGeo4W\apps\grass\grass-7.1.svn'
grass7bin_win = r'C:\OSGeo4W\bin\grass71svn.bat'
# Linux
grass7bin_lin = 'grass70'
# MacOSX
grass7bin_mac = '/Applications/GRASS/GRASS-7.1.app/'
#myepsg = '4326' # latlong
myepsg = '32632' # ETRS-TM32, http://spatialreference.org/ref/epsg/3044/
#myfile = '/home/neteler/markus_repo/books/kluwerbook/data3rd/lidar/lidar_raleigh_nc_spm.shp'
#myfile = '/data/maps/world_natural_earth_250m/europe_north_east.tif'
#myfile = '/home/f/Documents/triglav/ecological.asc'
#myfile = r'C:\Dati\Padergnone\square_p95.tif'



########### SOFTWARE
if sys.platform.startswith('linux'):
    # we assume that the GRASS GIS start script is available and in the PATH
    # query GRASS 7 itself for its GISBASE
    grass7bin = grass7bin_lin
elif sys.platform.startswith('win'):
    grass7bin = grass7bin_win
else:
    OSError('Platform not configured.')
 
startcmd = grass7bin + ' --config path'
 
p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, "ERROR: Cannot find GRASS GIS 7 start script (%s)" % startcmd
    sys.exit(-1)
home = expanduser("~")

if sys.platform.startswith('linux'):
    gisbase = out.strip('\n')
elif sys.platform.startswith('win'):
    if out.find("OSGEO4W home is") != -1:
        gisbase = out.strip().split('\n')[1]
    else:
        gisbase = out.strip('\n')
    os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin', 'sh.exe')
 
# Set GISBASE environment variable
os.environ['GISBASE'] = gisbase

# the following not needed with trunk
os.environ['PATH'] += os.pathsep + os.path.join(gisbase, 'extrabin')
os.environ['PATH'] += os.pathsep + os.path.join(home, '.grass7', 'addons', 'scripts')

# define GRASS-Python environment
gpydir = os.path.join(gisbase, "etc", "python")
sys.path.append(gpydir)
########
# define GRASS DATABASE

"""
if sys.platform.startswith('win'):
    gisdb = os.path.join(os.getenv('APPDATA', 'grassdata')
else:
    gisdb = os.path.join(os.getenv('HOME', 'grassdata')
""" 
# override for now with TEMP dir
gisdb = os.path.join(tempfile.gettempdir(), 'grassdata')
try:
    os.stat(gisdb)
except:
    os.mkdir(gisdb)
 
# location/mapset: use random names for batch jobs
string_length = 16
location = binascii.hexlify(os.urandom(string_length))
mapset   = 'PERMANENT'
location_path = os.path.join(gisdb, location)
 
# Create new location (we assume that grass7bin is in the PATH)
#  from EPSG code:
startcmd = grass7bin + ' -c epsg:' + myepsg + ' -e ' + location_path
#  from SHAPE or GeoTIFF file
#startcmd = grass7bin + ' -c ' + myfile + ' -e ' + location_path
 
#print startcmd
p = subprocess.Popen(startcmd, shell=True, 
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = p.communicate()
#if p.returncode != 0:
    #print >>sys.stderr, 'ERROR: %s' % err
    #print >>sys.stderr, 'ERROR: Cannot generate location (%s)' % startcmd
    #sys.exit(-1)
#else:
    #print 'Created location %s' % location_path
 
# Now the location with PERMANENT mapset exists.
 
########
# Now we can use PyGRASS or GRASS Scripting library etc. after 
# having started the session with gsetup.init() etc
 
# Set GISDBASE environment variable
os.environ['GISDBASE'] = gisdb
 
# Linux: Set path to GRASS libs (TODO: NEEDED?)
path = os.getenv('LD_LIBRARY_PATH')
dir  = os.path.join(gisbase, 'lib')
if path:
    path = dir + os.pathsep + path
else:
    path = dir
os.environ['LD_LIBRARY_PATH'] = path
 
# language
os.environ['LANG'] = 'en_US'
os.environ['LOCALE'] = 'C'
 
# Windows: NEEDED?
#path = os.getenv('PYTHONPATH')
#dirr = os.path.join(gisbase, 'etc', 'python')
#if path:
#    path = dirr + os.pathsep + path
#else:
#    path = dirr
#os.environ['PYTHONPATH'] = path
 
#print os.environ
 
## Import GRASS Python bindings
import grass.script as grass
import grass.script.setup as gsetup
from grass.script.core import run_command, parser, overwrite, read_command


class MyMessageBox(QtGui.QWidget):
     def __init__(self, parent=None):
         QtGui.QWidget.__init__(self, parent)
         self.setGeometry(300, 300, 250, 150)
         self.setWindowTitle('message box')


class Worker_theoretical(QtCore.QObject):

    def __init__(self,dlg,listalayers):
        QtCore.QObject.__init__(self)
        self.dlg=dlg 
        self.listalayers=listalayers


    def run(self):

        ow = overwrite()
        #q.put([42, None, 'hello'])

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 

        
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])


        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])



        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        increment_field = str(self.dlg.combo_increment.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix.text()


        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())



        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


 
        run_command("g.remove", type="vector", name="forest",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", dsn=parcel, output="forest",overwrite=ow,flags = 'o') 

        run_command("g.remove", type="vector", name="boundaries",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", dsn=boundaries, output="boundaries",overwrite=ow,flags = 'o')


        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="increment", use="attr", attrcolumn=increment_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=treatment_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field,overwrite=True)

        #g.region vect=forest@PERMANENT res=5



        #r.green processing

        run_command("r.green.biomassfor.theoretical",energy_tops_hf=energy_tops_hf,energy_cormometric_vol_hf=energy_cormometric_vol_hf,
                    increment='increment',yield_surface="yield_surface",management="management",treatment="treatment",
                    output_prefix=prefix_output,overwrite=True)

        maptot=prefix_output+"_p_bioenergy"
        maphf=prefix_output+"_p_bioenergyHF"
        mapc=prefix_output+"_p_bioenergyC"

        #export map

        run_command("r.out.gdal",input=maptot,output=output_directory+"/"+maptot+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=maphf,output=output_directory+"/"+maphf+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=mapc,output=output_directory+"/"+mapc+".tif",format="GTiff",overwrite=True)


        #statistic=read_command("r.univar",rast=maptot)

        """
        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 
        """


        tempo=time.time() - start_time

        self.dlg.console_r.appendPlainText("Processing executed\n\n")
        self.dlg.console_r.appendPlainText("Process time: "+str(tempo))
        self.dlg.console_r.appendPlainText("\n\n")
        self.dlg.console_r.appendPlainText("Resulted maps:\n")
        self.dlg.console_r.appendPlainText("1) "+maptot)
        self.dlg.console_r.appendPlainText("2) "+maphf)
        self.dlg.console_r.appendPlainText("3) "+mapc)
        self.dlg.console_r.appendPlainText("\n\n")

        self.dlg.console_r.appendPlainText("---------------------------------\n\n")



    finished = QtCore.pyqtSignal(object)
    error = QtCore.pyqtSignal(Exception, basestring)
    progress = QtCore.pyqtSignal(float)




class Worker_legal(QtCore.QObject):

    def __init__(self,dlg,listalayers):
        QtCore.QObject.__init__(self)
        self.dlg=dlg 
        self.listalayers=listalayers


    def run(self):

        ow = overwrite()
        #q.put([42, None, 'hello'])

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 

        
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])


        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])



        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_legal.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_2.text()


        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())



        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        run_command("g.remove", type="vector", name="forest",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", dsn=parcel, output="forest",overwrite=ow,flags = 'o') 

        run_command("g.remove", type="vector", name="boundaries",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=boundaries, output="boundaries",overwrite=ow,flags = 'o')



        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="yield", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field,overwrite=True)

        #g.region vect=forest@PERMANENT res=5



        #r.green processing

        run_command("r.green.biomassfor.legal",energy_tops_hf=energy_tops_hf,energy_cormometric_vol_hf=energy_cormometric_vol_hf,
                    yield1='yield',yield_surface="yield_surface",management="management",treatment="treatment",
                    output_prefix=prefix_output,overwrite=True)

        maptot=prefix_output+"_l_bioenergy"
        maphf=prefix_output+"_l_bioenergyHF"
        mapc=prefix_output+"_l_bioenergyC"

        #export map

        run_command("r.out.gdal",input=maptot,output=output_directory+"/"+maptot+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=maphf,output=output_directory+"/"+maphf+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=mapc,output=output_directory+"/"+mapc+".tif",format="GTiff",overwrite=True)


        #statistic=read_command("r.univar",rast=maptot)

        """
        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 
        """


        tempo=time.time() - start_time

        self.dlg.console_l.appendPlainText("Processing executed\n\n")
        self.dlg.console_l.appendPlainText("Process time: "+str(tempo))
        self.dlg.console_l.appendPlainText("\n\n")
        self.dlg.console_l.appendPlainText("Resulted maps:\n")
        self.dlg.console_l.appendPlainText("1) "+maptot)
        self.dlg.console_l.appendPlainText("2) "+maphf)
        self.dlg.console_l.appendPlainText("3) "+mapc)
        self.dlg.console_l.appendPlainText("\n\n")

        self.dlg.console_l.appendPlainText("---------------------------------\n\n")



    finished = QtCore.pyqtSignal(object)
    error = QtCore.pyqtSignal(Exception, basestring)
    progress = QtCore.pyqtSignal(float)




class Worker_tech(QtCore.QObject):

    def __init__(self,dlg,listalayers):
        QtCore.QObject.__init__(self)
        self.dlg=dlg 
        self.listalayers=listalayers


    def run(self):

        ow = overwrite()
        #q.put([42, None, 'hello'])

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 

        
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])


        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])

        froads_text = str(self.dlg.combo_froads.currentText())
        froads_layer=self.listalayers[froads_text]
        froads_path=string.split(froads_layer.dataProvider().dataSourceUri(), "|")
        froads=str(froads_path[0])


        dem_text = str(self.dlg.combo_dem.currentText())
        dem_layer=self.listalayers[dem_text]
        dem_path=string.split(dem_layer.dataProvider().dataSourceUri(), "|")
        dem=str(dem_path[0])


        #dem= str(self.dlg.combo_dem.currentText())
        #froads= str(self.dlg.combo_froads.currentText())


        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_tech.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_3.text()


        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())



        roughness_field = str(self.dlg.comborough.currentText())
        rivers_field = str(self.dlg.comborivers.currentText())
        lakes_field = str(self.dlg.combolakes.currentText())
        mm_field = str(self.dlg.combomm.currentText())


        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        run_command("g.remove", type="vector", name="forest",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", dsn=parcel, output="forest",overwrite=ow,flags = 'o') 

        run_command("g.remove", type="vector", name="boundaries",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=boundaries, output="boundaries",overwrite=ow,flags = 'o')


        run_command("g.remove", type="vector", name="froads",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=froads, output="froads",overwrite=ow,flags = 'o')


        run_command("g.remove", type="raster", name="dem",overwrite=ow,flags = 'f') 
        run_command("r.in.gdal", input=dem, output="dem",overwrite=ow,flags = 'o')



        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="yield", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field,overwrite=True)

        run_command("v.to.rast", input='froads',output="froads", use="val", overwrite=True)

        run_command("r.null", map='yield',null=0)
        run_command("r.null", map='management',null=0)
        run_command("r.null", map='treatment',null=0)
        run_command("r.null", map='yield_surface',null=0)
        run_command("r.null", map='froads',null=0)


        if roughness_field!="No required":
            run_command("v.to.rast", input='forest',output="roughness", use="attr", attrcolumn=roughness_field,overwrite=True)
            run_command("r.null", map='roughness',null=0)
        else:
            roughness_field=""


        if rivers_field!="No required":
            rivers_layer=self.listalayers[rivers_field]
            rivers_path=string.split(rivers_layer.dataProvider().dataSourceUri(), "|")
            rivers=str(rivers_path[0])
            run_command("g.remove", type="vector", name="rivers",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=rivers, output="rivers",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='rivers',output="rivers", use="val", overwrite=True)
            run_command("r.null", map='rivers',null=0)
        else:
            rivers_field=""


        if lakes_field!="No required":
            lakes_layer=self.listalayers[lakes_field]
            lakes_path=string.split(lakes_layer.dataProvider().dataSourceUri(), "|")
            lakes=str(lakes_path[0])
            run_command("g.remove", type="vector", name="lakes",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=lakes, output="lakes",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='lakes',output="lakes", use="val", overwrite=True)
            run_command("r.null", map='lakes',null=0)
        else:
            lakes_field=""

        """
        if mm_field=="No required":
            mm_layer=self.listalayers[mm_field]
            mm_path=string.split(mm_layer.dataProvider().dataSourceUri(), "|")
            mm=str(mm_path[0])
            run_command("g.remove", type="vector", name="mm",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=mm, output="mm",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='mm',output="mm", use="val", overwrite=True)
            run_command("r.null", map='mm',null=0)
        else:
            mm_field=""
        """




        #g.region vect=forest@PERMANENT res=5


        cc_min_slope= self.dlg.spin_cc_minslope.text()
        cc_max_slope= self.dlg.spin_cc_maxslope.text()
        cc_max_dist= self.dlg.spin_cc_maxdist.text()

        for_max_slope= self.dlg.spin_for_maxslope.text()
        for_max_dist= self.dlg.spin_for_maxdist.text()

        oth_max_slope= self.dlg.spin_oth_maxslope.text()
        oth_max_dist= self.dlg.spin_oth_maxdist.text()

        #r.green processing

        run_command("r.green.biomassfor.technical",forest="forest",dtm="dem",forest_roads="froads",energy_tops_hf=energy_tops_hf,
                    energy_cormometric_vol_hf=energy_cormometric_vol_hf,slp_min_cc=cc_min_slope,slp_max_cc=cc_max_slope,
                    dist_max_cc=cc_max_dist,slp_max_fw=for_max_slope,dist_max_fw=for_max_dist,slp_max_cop=oth_max_slope,dist_max_cop=oth_max_dist,
                    yield1='yield',yield_surface="yield_surface",management="management",treatment="treatment",rivers=rivers_field,lakes=lakes_field,roughness=roughness_field,
                    output_prefix=prefix_output,overwrite=True)

        maptot=prefix_output+"_tech_bioenergy"
        maphf=prefix_output+"_tech_bioenergyHF"
        mapc=prefix_output+"_tech_bioenergyC"

        #export map

        run_command("r.out.gdal",input=maptot,output=output_directory+"/"+maptot+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=maphf,output=output_directory+"/"+maphf+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=mapc,output=output_directory+"/"+mapc+".tif",format="GTiff",overwrite=True)


        #statistic=read_command("r.univar",rast=maptot)

        """
        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 
        """


        tempo=time.time() - start_time

        self.dlg.console_t.appendPlainText("Processing executed\n\n")
        self.dlg.console_t.appendPlainText("Process time: "+str(tempo))
        self.dlg.console_t.appendPlainText("\n\n")
        self.dlg.console_t.appendPlainText("Resulted maps:\n")
        self.dlg.console_t.appendPlainText("1) "+maptot)
        self.dlg.console_t.appendPlainText("2) "+maphf)
        self.dlg.console_t.appendPlainText("3) "+mapc)
        self.dlg.console_t.appendPlainText("\n\n")

        self.dlg.console_t.appendPlainText("---------------------------------\n\n")



    finished = QtCore.pyqtSignal(object)
    error = QtCore.pyqtSignal(Exception, basestring)
    progress = QtCore.pyqtSignal(float)





class Worker_econ(QtCore.QObject):

    def __init__(self,dlg,listalayers):
        QtCore.QObject.__init__(self)
        self.dlg=dlg 
        self.listalayers=listalayers


    def run(self):

        ow = overwrite()
        #q.put([42, None, 'hello'])

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 

        
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])


        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])

        froads_text = str(self.dlg.combo_froads.currentText())
        froads_layer=self.listalayers[froads_text]
        froads_path=string.split(froads_layer.dataProvider().dataSourceUri(), "|")
        froads=str(froads_path[0])


        mroads_text = str(self.dlg.combo_mroads.currentText())
        mroads_layer=self.listalayers[mroads_text]
        mroads_path=string.split(mroads_layer.dataProvider().dataSourceUri(), "|")
        mroads=str(mroads_path[0])


        dhp_text = str(self.dlg.combo_dhp.currentText())
        dhp_layer=self.listalayers[dhp_text]
        dhp_path=string.split(dhp_layer.dataProvider().dataSourceUri(), "|")
        dhp=str(dhp_path[0])


        dem_text = str(self.dlg.combo_dem.currentText())
        dem_layer=self.listalayers[dem_text]
        dem_path=string.split(dem_layer.dataProvider().dataSourceUri(), "|")
        dem=str(dem_path[0])


        #dem= str(self.dlg.combo_dem.currentText())
        #froads= str(self.dlg.combo_froads.currentText())


        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_econ.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_4.text()


        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())



        roughness_field = str(self.dlg.comborough.currentText())
        rivers_field = str(self.dlg.comborivers.currentText())
        lakes_field = str(self.dlg.combolakes.currentText())
        mm_field = str(self.dlg.combomm.currentText())


        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        run_command("g.remove", type="vector", name="forest",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", dsn=parcel, output="forest",overwrite=ow,flags = 'o') 

        run_command("g.remove", type="vector", name="boundaries",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=boundaries, output="boundaries",overwrite=ow,flags = 'o')


        run_command("g.remove", type="vector", name="froads",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=froads, output="froads",overwrite=ow,flags = 'o')


        run_command("g.remove", type="vector", name="mroads",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=mroads, output="mroads",overwrite=ow,flags = 'o')

        run_command("g.remove", type="vector", name="dhp",overwrite=ow,flags = 'f') 
        run_command("v.in.ogr", input=dhp, output="dhp",overwrite=ow,flags = 'o')


        run_command("g.remove", type="raster", name="dem",overwrite=ow,flags = 'f') 
        run_command("r.in.gdal", input=dem, output="dem",overwrite=ow,flags = 'o')



        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="yield", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=yield_field,overwrite=True)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field,overwrite=True)

        run_command("v.to.rast", input='froads',output="froads", use="val", overwrite=True)

        run_command("v.to.rast", input='mroads',output="mroads", use="val", overwrite=True)

        run_command("r.null", map='yield',null=0)
        run_command("r.null", map='management',null=0)
        run_command("r.null", map='treatment',null=0)
        run_command("r.null", map='yield_surface',null=0)
        run_command("r.null", map='froads',null=0)
        run_command("r.null", map='mroads',null=0)


        if roughness_field!="No required":
            run_command("v.to.rast", input='forest',output="roughness", use="attr", attrcolumn=roughness_field,overwrite=True)
            run_command("r.null", map='roughness',null=0)
        else:
            roughness_field=""


        if rivers_field!="No required":
            rivers_layer=self.listalayers[rivers_field]
            rivers_path=string.split(rivers_layer.dataProvider().dataSourceUri(), "|")
            rivers=str(rivers_path[0])
            run_command("g.remove", type="vector", name="rivers",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=rivers, output="rivers",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='rivers',output="rivers", use="val", overwrite=True)
            run_command("r.null", map='rivers',null=0)
        else:
            rivers_field=""


        if lakes_field!="No required":
            lakes_layer=self.listalayers[lakes_field]
            lakes_path=string.split(lakes_layer.dataProvider().dataSourceUri(), "|")
            lakes=str(lakes_path[0])
            run_command("g.remove", type="vector", name="lakes",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=lakes, output="lakes",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='lakes',output="lakes", use="val", overwrite=True)
            run_command("r.null", map='lakes',null=0)
        else:
            lakes_field=""

        """
        if mm_field=="No required":
            mm_layer=self.listalayers[mm_field]
            mm_path=string.split(mm_layer.dataProvider().dataSourceUri(), "|")
            mm=str(mm_path[0])
            run_command("g.remove", type="vector", name="mm",overwrite=ow,flags = 'f')
            run_command("v.in.ogr", input=mm, output="mm",overwrite=ow,flags = 'o')
            run_command("v.to.rast", input='mm',output="mm", use="val", overwrite=True)
            run_command("r.null", map='mm',null=0)
        else:
            mm_field=""
        """




        #g.region vect=forest@PERMANENT res=5


        cc_min_slope= self.dlg.spin_cc_minslope.text()
        cc_max_slope= self.dlg.spin_cc_maxslope.text()
        cc_max_dist= self.dlg.spin_cc_maxdist.text()

        for_max_slope= self.dlg.spin_for_maxslope.text()
        for_max_dist= self.dlg.spin_for_maxdist.text()

        oth_max_slope= self.dlg.spin_oth_maxslope.text()
        oth_max_dist= self.dlg.spin_oth_maxdist.text()

        market_price=str(self.dlg.market_price.text())


        en_woodchip= float(self.dlg.energy_woodchip.text()) if self.dlg.energy_woodchip.text()!="" else 20
        p_chainsaw= float(self.dlg.chainsaw.text()) if self.dlg.chainsaw.text()!="" else 0
        p_processor= float(self.dlg.processor.text()) if self.dlg.processor.text()!="" else 0
        p_forwarder= float(self.dlg.forwarder.text()) if self.dlg.forwarder.text()!="" else 0
        p_skidder= float(self.dlg.skidder.text()) if self.dlg.skidder.text()!="" else 0
        p_hp_cc= float(self.dlg.hp_cc.text()) if self.dlg.hp_cc.text()!="" else 0
        p_mp_cc= float(self.dlg.mp_cc.text()) if self.dlg.mp_cc.text()!="" else 0
        p_harvester= float(self.dlg.harvester.text()) if self.dlg.harvester.text()!="" else 0
        p_chipping= float(self.dlg.chipping.text()) if self.dlg.chipping.text()!="" else 0
        p_transport= float(self.dlg.transport.text()) if self.dlg.transport.text()!="" else 0

        prefix_voltypx= self.dlg.line_voltypx.text()

        pricelist=string.split(market_price,',')

        if prefix_voltypx!="" and len(pricelist)>1:
            
            for x in range(1,len(pricelist)+1):
                attr_map=prefix_voltypx+"_voltyp"+str(x)
                run_command("v.to.rast", input='forest',output="price_voltyp"+str(x), use="attr", attrcolumn= attr_map,overwrite=True)
                run_command("r.null", map="price_voltyp"+str(x),null=0)

        else:
            run_command("v.to.rast", input='forest',output="price_voltyp1", use="attr", attrcolumn= yield_field,overwrite=True)
            run_command("r.null", map='price_voltyp1',null=0)




        #r.green processing

        run_command("r.green.biomassfor.economic",flags="x", field_prefix="price",prices=pricelist,dhp="dhp",forest="forest",dtm="dem",forest_roads="froads",energy_tops_hf=energy_tops_hf,
                    energy_cormometric_vol_hf=energy_cormometric_vol_hf,slp_min_cc=cc_min_slope,slp_max_cc=cc_max_slope,main_roads="mroads",
                    dist_max_cc=cc_max_dist,slp_max_fw=for_max_slope,dist_max_fw=for_max_dist,slp_max_cop=oth_max_slope,dist_max_cop=oth_max_dist,
                    yield1='yield',yield_surface="yield_surface",management="management",treatment="treatment",rivers=rivers_field,lakes=lakes_field,roughness=roughness_field,
                    output_prefix=prefix_output,price_energy_woodchips=en_woodchip,cost_chainsaw=p_chainsaw,cost_forwarder=p_forwarder,cost_processor=p_processor,
                    cost_skidder=p_skidder,cost_harvester=p_harvester,cost_chipping=p_chipping,cost_transport=p_transport,overwrite=True)

        maptot=prefix_output+"_econ_bioenergy"
        maphf=prefix_output+"_econ_bioenergyHF"
        mapc=prefix_output+"_econ_bioenergyC"

        #export map

        run_command("r.out.gdal",input=maptot,output=output_directory+"/"+maptot+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=maphf,output=output_directory+"/"+maphf+".tif",format="GTiff",overwrite=True)
        run_command("r.out.gdal",input=mapc,output=output_directory+"/"+mapc+".tif",format="GTiff",overwrite=True)


        #statistic=read_command("r.univar",rast=maptot)

        """
        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 
        """


        tempo=time.time() - start_time

        self.dlg.console_e.appendPlainText("Processing executed\n\n")
        self.dlg.console_e.appendPlainText("Process time: "+str(tempo))
        self.dlg.console_e.appendPlainText("\n\n")
        self.dlg.console_e.appendPlainText("Resulted maps:\n")
        self.dlg.console_e.appendPlainText("1) "+maptot)
        self.dlg.console_e.appendPlainText("2) "+maphf)
        self.dlg.console_e.appendPlainText("3) "+mapc)
        self.dlg.console_e.appendPlainText("\n\n")

        self.dlg.console_e.appendPlainText("---------------------------------\n\n")



    finished = QtCore.pyqtSignal(object)
    error = QtCore.pyqtSignal(Exception, basestring)
    progress = QtCore.pyqtSignal(float)



class rg_biomass:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas=self.iface.mapCanvas()
        self.msgBar = self.iface.messageBar()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'rg_biomass_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = rg_biomassDialog()
        self.dlg.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint |
            QtCore.Qt.WindowSystemMenuHint)

        self.dlg.helpButton_th.setIcon(QIcon(os.path.dirname(__file__)+'/helpicon.png'))
        self.dlg.helpButton_th.setIconSize(QSize(24,24))
        self.dlg.helpButton_leg.setIcon(QIcon(os.path.dirname(__file__)+'/helpicon.png'))
        self.dlg.helpButton_leg.setIconSize(QSize(24,24))
        self.dlg.helpButton_tech.setIcon(QIcon(os.path.dirname(__file__)+'/helpicon.png'))
        self.dlg.helpButton_tech.setIconSize(QSize(24,24))
        self.dlg.helpButton_econ.setIcon(QIcon(os.path.dirname(__file__)+'/helpicon.png'))
        self.dlg.helpButton_econ.setIconSize(QSize(24,24))

        self.dlg.spin_res.setRange(1, 999); 
        self.dlg.spin_res.setValue(1)


        self.dlg.spin_cc_minslope.setRange(1, 100); 
        self.dlg.spin_cc_minslope.setValue(30)

        self.dlg.spin_cc_maxslope.setRange(1, 100); 
        self.dlg.spin_cc_maxslope.setValue(100)

        self.dlg.spin_cc_maxdist.setRange(1, 10000); 
        self.dlg.spin_cc_maxdist.setValue(800)

        self.dlg.spin_for_maxslope.setRange(1, 100); 
        self.dlg.spin_for_maxslope.setValue(30)

        self.dlg.spin_for_maxdist.setRange(1, 10000); 
        self.dlg.spin_for_maxdist.setValue(600)


        self.dlg.spin_oth_maxslope.setRange(1, 100); 
        self.dlg.spin_oth_maxslope.setValue(30)

        self.dlg.spin_oth_maxdist.setRange(1, 10000); 
        self.dlg.spin_oth_maxdist.setValue(600)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&R.green')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'rg_biomassfor')
        self.toolbar.setObjectName(u'rg_biomassfor')



        #self.dlg.button_box_th.accepted.connect(self.theoretical)
        self.dlg.button_box_th.accepted.connect(self.process_theoretical)
        self.dlg.button_box_th.button(QtGui.QDialogButtonBox.Reset).clicked.connect(lambda:self.reset("theoretical"))
        
        self.dlg.button_box_leg.accepted.connect(self.process_legal)
        self.dlg.button_box_leg.button(QtGui.QDialogButtonBox.Reset).clicked.connect(lambda:self.reset("legal"))
        #self.dlg.button_box_th.reset.connect(self.reset_field)

        self.dlg.button_box_tech.accepted.connect(self.process_tech)
        self.dlg.button_box_tech.button(QtGui.QDialogButtonBox.Reset).clicked.connect(lambda:self.reset("tech"))
        #self.dlg.button_box_th.reset.connect(self.reset_field)

        self.dlg.button_box_econ.accepted.connect(self.process_econ)
        self.dlg.button_box_econ.button(QtGui.QDialogButtonBox.Reset).clicked.connect(lambda:self.reset("econ"))


        self.dlg.comboforest.currentIndexChanged[str].connect(self.reset_combo)
        self.dlg.button_save_folder.clicked.connect(self.choose_out_folder)
        #self.dlg.button_box_th.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.reset_field)


        ###### help button connections #####
        self.dlg.helpButton_th.clicked.connect(lambda: self.help("r.green.biomassfor.theoretical.html"))
        self.dlg.helpButton_leg.clicked.connect(lambda: self.help("r.green.biomassfor.legal.html"))
        self.dlg.helpButton_tech.clicked.connect(lambda: self.help("r.green.biomassfor.technical.html"))
        self.dlg.helpButton_econ.clicked.connect(lambda: self.help("r.green.biomassfor.economic.html"))


    def help(self,rgreenfunction):         
        grasspath = grass7bin + ' --config path'
        grasspath1 = subprocess.Popen(grasspath, shell=True)
        gpath, gerr = grasspath1.communicate()
        gpath=commands.getoutput(grass7bin+' --config path')

        if platform.uname()[0]=="Windows":
            os.system("start "+gpath+"/docs/html/"+rgreenfunction)
        if platform.uname()[0]=="Linux":
            os.system("xdg-open "+gpath+"/docs/html/"+rgreenfunction)
        else:
            os.system("open "+gpath+"/docs/html/"+rgreenfunction)



    def reset(self,sub_module):
        if sub_module=="theoretical":
            self.dlg.console_r.clear()
        if sub_module=="legal":
            self.dlg.console_l.clear()
        if sub_module=="tech":
            self.dlg.console_t.clear()
        if sub_module=="econ":
            self.dlg.console_e.clear()



    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('rg_biomass', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/rg_biomass/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'R.green.biomassfor'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&r.green.biomassfor'),
                action)
            self.iface.removeToolBarIcon(action)


    def choose_out_folder(self):
        fname = QFileDialog.getExistingDirectory(None,"Open a folder","/",QFileDialog.ShowDirsOnly)
        self.dlg.line_out_folder.setText(fname) 
        # if self.tipofile=="csv":
        #     self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','csv files (*.csv);;all files (*.*)')
        #     self.dlg_conf.path_to_kmean.setText(self.fname)            
        # if self.tipofile=="tif":
        #     self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','GeoTiff files (*.tif);;all files (*.*)')
        #     self.dlg_reclass.output_raster_class.setText(self.fname)            
    

    def process_theoretical(self):

        ######## check data ########

        forest_text = str(self.dlg.comboforest.currentText())
        boundaries_text = str(self.dlg.combostudy.currentText()) 
        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        increment_field = str(self.dlg.combo_increment.currentText())
        resolution=int(self.dlg.spin_res.text())
        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix.text()

        if prefix_output=="":
            QMessageBox.warning(self.dlg,"Warning", "Check prefix output data *" )
            return

        if forest_text=="" or boundaries_text=="" or area_field=="" or treatment_field=="" or management_field=="" or increment_field=="" or output_directory=="":
            QMessageBox.warning(self.dlg,"Warning", "Check mandatory data *" )
            return    
        energy_tops_hf= self.dlg.line_energy_tbhf.text()
        energy_cormometric_vol_hf= self.dlg.line_energy_whf.text()
        energy_tops_cop= self.dlg.line_energy_tbc.text()

        if energy_tops_hf=="" or energy_cormometric_vol_hf=="" or energy_tops_cop=="":
            QMessageBox.warning(self.dlg,"Warning", "Check energy data" )
            return 

        ###### end check data ######




        try:
            self.thread.quit()
            self.thread=None
        except:
            pass

        self.dlg.console_r.appendPlainText("***R.green.biomassfor.theoretical***\n\n")
        self.dlg.console_r.appendPlainText("starting time: "+str(time.strftime("%H:%M:%S"))+"\n\n")
        self.dlg.console_r.appendPlainText("Process executing.......wait please\n\n")
        worker = Worker_theoretical(self.dlg,self.listalayers)
        thread = QtCore.QThread()
        worker.moveToThread(thread)
        worker.finished.connect(self.workerFinished)
        worker.error.connect(self.workerError)
        thread.started.connect(worker.run)
        thread.start()
        self.thread = thread
        self.worker = worker


    def process_legal(self):

        ######## check data ########

        forest_text = str(self.dlg.comboforest.currentText())
        boundaries_text = str(self.dlg.combostudy.currentText()) 
        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_legal.currentText())
        resolution=int(self.dlg.spin_res.text())
        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_2.text()

        if prefix_output=="":
            QMessageBox.warning(self.dlg,"Warning", "Check prefix output data *" )
            return

        if forest_text=="" or boundaries_text=="" or area_field=="" or treatment_field=="" or management_field=="" or yield_field=="" or output_directory=="":
            QMessageBox.warning(self.dlg,"Warning", "Check mandatory data *" )
            return    
        energy_tops_hf= self.dlg.line_energy_tbhf.text()
        energy_cormometric_vol_hf= self.dlg.line_energy_whf.text()
        energy_tops_cop= self.dlg.line_energy_tbc.text()

        if energy_tops_hf=="" or energy_cormometric_vol_hf=="" or energy_tops_cop=="":
            QMessageBox.warning(self.dlg,"Warning", "Check energy data" )
            return 

        ###### end check data ######




        try:
            self.thread.quit()
            self.thread=None
        except:
            pass

        self.dlg.console_l.appendPlainText("***R.green.biomassfor.legal***\n\n")
        self.dlg.console_l.appendPlainText("starting time: "+str(time.strftime("%H:%M:%S"))+"\n\n")
        self.dlg.console_l.appendPlainText("Process executing.......wait please\n\n")
        worker = Worker_legal(self.dlg,self.listalayers)
        thread = QtCore.QThread()
        worker.moveToThread(thread)
        worker.finished.connect(self.workerFinished)
        worker.error.connect(self.workerError)
        thread.started.connect(worker.run)
        thread.start()
        self.thread = thread
        self.worker = worker



    def process_tech(self):

        ######## check data ########

        forest_text = str(self.dlg.comboforest.currentText())
        boundaries_text = str(self.dlg.combostudy.currentText()) 
        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_tech.currentText())
        resolution=int(self.dlg.spin_res.text())
        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_3.text()

        dem= str(self.dlg.combo_dem.currentText())
        froads= str(self.dlg.combo_froads.currentText())

        if prefix_output=="":
            QMessageBox.warning(self.dlg,"Warning", "Check prefix output data *" )
            return

        if forest_text=="" or boundaries_text=="" or area_field=="" or treatment_field=="" or management_field=="" or yield_field=="" or output_directory=="":
            QMessageBox.warning(self.dlg,"Warning", "Check mandatory data *" )
            return    

        if dem=="":
            QMessageBox.warning(self.dlg,"Warning", "Digital Elevation Model is required *" )
            return 

        if froads=="":
            QMessageBox.warning(self.dlg,"Warning", "Forest roads vector map is required *" )
            return

        energy_tops_hf= self.dlg.line_energy_tbhf.text()
        energy_cormometric_vol_hf= self.dlg.line_energy_whf.text()
        energy_tops_cop= self.dlg.line_energy_tbc.text()



        cc_min_slope= self.dlg.spin_cc_minslope.text()
        cc_max_slope= self.dlg.spin_cc_maxslope.text()
        cc_max_dist= self.dlg.spin_cc_maxdist.text()

        for_max_slope= self.dlg.spin_for_maxslope.text()
        for_max_dist= self.dlg.spin_for_maxdist.text()

        oth_max_slope= self.dlg.spin_oth_maxslope.text()
        oth_max_dist= self.dlg.spin_oth_maxdist.text()

        if energy_tops_hf=="" or energy_cormometric_vol_hf=="" or energy_tops_cop=="":
            QMessageBox.warning(self.dlg,"Warning", "Check energy data" )
            return 

        ###### end check data ######




        try:
            self.thread.quit()
            self.thread=None
            #print "cancellato"
        except:
            pass

        self.dlg.console_t.appendPlainText("***R.green.biomassfor.technical***\n\n")
        self.dlg.console_t.appendPlainText("starting time: "+str(time.strftime("%H:%M:%S"))+"\n\n")
        self.dlg.console_t.appendPlainText("Process executing.......wait please\n\n")
        worker = Worker_tech(self.dlg,self.listalayers)
        thread = QtCore.QThread()
        worker.moveToThread(thread)
        worker.finished.connect(self.workerFinished)
        worker.error.connect(self.workerError)
        thread.started.connect(worker.run)
        thread.start()
        self.thread = thread
        self.worker = worker


    def process_econ(self):

        ######## check data ########

        forest_text = str(self.dlg.comboforest.currentText())
        boundaries_text = str(self.dlg.combostudy.currentText()) 
        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_econ.currentText())
        resolution=int(self.dlg.spin_res.text())
        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix_4.text()

        dem= str(self.dlg.combo_dem.currentText())
        froads= str(self.dlg.combo_froads.currentText())

        mroads= str(self.dlg.combo_mroads.currentText())

        dhp= str(self.dlg.combo_dhp.currentText())

        if prefix_output=="":
            QMessageBox.warning(self.dlg,"Warning", "Check prefix output data *" )
            return

        if forest_text=="" or boundaries_text=="" or area_field=="" or treatment_field=="" or management_field=="" or yield_field=="" or output_directory=="":
            QMessageBox.warning(self.dlg,"Warning", "Check mandatory data *" )
            return    

        if dem=="":
            QMessageBox.warning(self.dlg,"Warning", "Digital Elevation Model is required *" )
            return 

        if froads=="" or mroads=="":
            QMessageBox.warning(self.dlg,"Warning", "Forest and main roads vector map are required *" )
            return

        if dhp=="":
            QMessageBox.warning(self.dlg,"Warning", "District heating plants are required *" )
            return 

        energy_tops_hf= self.dlg.line_energy_tbhf.text()
        energy_cormometric_vol_hf= self.dlg.line_energy_whf.text()
        energy_tops_cop= self.dlg.line_energy_tbc.text()

        market_price=str(self.dlg.market_price.text())


        #market_price

        if market_price=="":
            QMessageBox.warning(self.dlg,"Warning", "At least one wood market price is required *" )
            return


        cc_min_slope= self.dlg.spin_cc_minslope.text()
        cc_max_slope= self.dlg.spin_cc_maxslope.text()
        cc_max_dist= self.dlg.spin_cc_maxdist.text()

        for_max_slope= self.dlg.spin_for_maxslope.text()
        for_max_dist= self.dlg.spin_for_maxdist.text()

        oth_max_slope= self.dlg.spin_oth_maxslope.text()
        oth_max_dist= self.dlg.spin_oth_maxdist.text()

        if energy_tops_hf=="" or energy_cormometric_vol_hf=="" or energy_tops_cop=="":
            QMessageBox.warning(self.dlg,"Warning", "Check energy data" )
            return 






        ###### end check data ######




        try:
            self.thread.quit()
            self.thread=None
            #print "cancellato"
        except:
            pass

        self.dlg.console_e.appendPlainText("***R.green.biomassfor.economic***\n\n")
        self.dlg.console_e.appendPlainText("starting time: "+str(time.strftime("%H:%M:%S"))+"\n\n")
        self.dlg.console_e.appendPlainText("Process executing.......wait please\n\n")
        worker = Worker_econ(self.dlg,self.listalayers)
        thread = QtCore.QThread()
        worker.moveToThread(thread)
        worker.finished.connect(self.workerFinished)
        worker.error.connect(self.workerError)
        thread.started.connect(worker.run)
        thread.start()
        self.thread = thread
        self.worker = worker


    def workerFinished(self):
        self.thread.quit()
        self.thread.wait()

        

    def workerError(self):
        self.iface.messageBar()('Errore:\n')


    def reset_field(self):
        self.dlg.comboforest.clear()
        self.dlg.combosurface.clear()
        self.dlg.combostudy.clear()
        self.dlg.combomanag.clear()
        self.dlg.combotreat.clear()
        self.dlg.comborivers.clear()
        self.dlg.combolakes.clear()
        self.dlg.combomm.clear()
        self.dlg.combosoilprod.clear()
        self.dlg.combotreevol.clear()
        self.dlg.combotreediam.clear()
        self.dlg.combo_froads.clear()
        self.dlg.combo_mroads.clear()
        self.dlg.combo_dhp.clear()


        self.dlg.line_energy_tbhf.setText("0.49")
        self.dlg.line_energy_whf.setText("1.97")
        self.dlg.line_energy_tbc.setText("0.55")

        self.dlg.energy_woodchip.setText("20")
        self.dlg.chainsaw.setText("13.17")
        self.dlg.processor.setText("87.42")
        self.dlg.forwarder.setText("70.70")
        self.dlg.skidder.setText("64.36")
        self.dlg.hp_cc.setText("111.44")
        self.dlg.mp_cc.setText("104.31")
        self.dlg.harvester.setText("96.33")
        self.dlg.chipping.setText("150.87")
        self.dlg.transport.setText("64.90")


        self.allLayers = self.canvas.layers()
        self.listalayers=dict()
        self.elementovuoto="No required"
        self.dlg.comborivers.addItem(self.elementovuoto)
        self.dlg.combolakes.addItem(self.elementovuoto)
        self.dlg.combomm.addItem(self.elementovuoto)
        self.dlg.combosoilprod.addItem(self.elementovuoto)
        self.dlg.combotreediam.addItem(self.elementovuoto)
        self.dlg.combotreevol.addItem(self.elementovuoto)
        for self.i in self.allLayers:
            self.listalayers[self.i.name()]=self.i
            if self.i.type() == QgsMapLayer.VectorLayer:            
                self.dlg.comboforest.addItem(str(self.i.name()))
                self.dlg.combostudy.addItem(str(self.i.name())) 
                self.dlg.combo_froads.addItem(str(self.i.name()))
                self.dlg.combo_mroads.addItem(str(self.i.name()))
                self.dlg.combo_dhp.addItem(str(self.i.name()))
                self.dlg.comborivers.addItem(str(self.i.name()))
                self.dlg.combolakes.addItem(str(self.i.name()))
                self.dlg.combomm.addItem(str(self.i.name()))
            if self.i.type() == QgsMapLayer.RasterLayer:
                self.dlg.combo_dem.addItem(str(self.i.name()))
                self.dlg.combosoilprod.addItem(str(self.i.name()))
                self.dlg.combotreediam.addItem(str(self.i.name()))
                self.dlg.combotreevol.addItem(str(self.i.name()))
            # if self.i.type() == QgsMapLayer.RasterLayer:           
            #     self.dlg.comboclimate.addItem(str(self.i.name()))
            #     self.dlg.comboaspect.addItem(str(self.i.name()))  



    def reset_combo(self):

        self.dlg.combosurface.clear()
        self.dlg.combomanag.clear()
        self.dlg.combotreat.clear()
        self.dlg.combo_increment.clear()
        self.dlg.combo_legal.clear()
        self.dlg.combo_tech.clear()
        self.dlg.combo_econ.clear()
        self.dlg.comborough.clear()

        self.elementovuoto="No required"
        self.dlg.comborough.addItem(self.elementovuoto)

        forest_text = str(self.dlg.comboforest.currentText()) 
        #print forest_text


        if forest_text!="":

            forest_layer=self.listalayers[forest_text]

            forest_fields = forest_layer.pendingFields()
            field_names = [field.name() for field in forest_fields]


            for text in field_names:
                self.dlg.combomanag.addItem(text)
                self.dlg.combotreat.addItem(text)
                self.dlg.combosurface.addItem(text)
                self.dlg.combo_increment.addItem(text)
                self.dlg.combo_legal.addItem(text)
                self.dlg.combo_tech.addItem(text)
                self.dlg.combo_econ.addItem(text)
                self.dlg.comborough.addItem(text)





    def legal(self):

        #recovery data from gui

        forest_text = str(self.dlg.comboforest.currentText()) 
        forest_layer=self.listalayers[forest_text]
        forest_path=string.split(forest_layer.dataProvider().dataSourceUri(), "|")
        parcel=str(forest_path[0])

        boundaries_text = str(self.dlg.combostudy.currentText()) 
        boundaries_layer=self.listalayers[boundaries_text]
        boundaries_path=string.split(boundaries_layer.dataProvider().dataSourceUri(), "|")
        boundaries=str(boundaries_path[0])


        area_field = str(self.dlg.combosurface.currentText())
        treatment_field = str(self.dlg.combotreat.currentText())
        management_field = str(self.dlg.combomanag.currentText())
        yield_field = str(self.dlg.combo_legal.currentText())

        resolution=int(self.dlg.spin_res.text())
        srs=self.dlg.line_srs.text()

        output_directory= self.dlg.line_out_folder.text()
        prefix_output= self.dlg.line_out_prefix.text()

        energy_tops_hf= float(self.dlg.line_energy_tbhf.text())
        energy_cormometric_vol_hf= float(self.dlg.line_energy_whf.text())
        energy_tops_cop= float(self.dlg.line_energy_tbc.text())


        # print parcel
        # print boundaries
        # print area_field
        # print treatment_field
        # print management_field
        # print yield_field
        # print resolution
        # print srs
        # print output_directory
        # print prefix_output
        # print energy_tops_hf
        # print energy_cormometric_vol_hf
        # print energy_tops_cop



        ###########


        # Launch session 
        gsetup.init(gisbase, gisdb, location, mapset)

        start_time = time.time()


        ############################
        ## start process r.green ##
        ############################


        #recovery data from gui
        # boundaries='/home/f/particellare_pnam/particellare.shp'
        # parcel='/home/f/particellare_pnam/particellare.shp'
        # yield_field='yield'
        # area_field='Hectares'
        # management_field='management'
        # treatment_field='treatment'
        # prefix_output='prova'
        # output_directory='/home/f/Desktop/'
        # resolution=5

        # energy_tops_hf=0.49
        # energy_cormometric_vol_hf=1.57
        # energy_tops_cop=0.55

        #grass map import

        run_command("v.in.ogr", dsn=parcel, output="forest",flags = 'o') 

        run_command("v.in.ogr", dsn=boundaries, output="boundaries",flags = 'o') 

        run_command("g.region",vect="boundaries",res=resolution)

        run_command("v.to.rast", input='forest',output="yield", use="attr", attrcolumn=yield_field)

        run_command("v.to.rast", input='forest',output="management", use="attr", attrcolumn=management_field)

        run_command("v.to.rast", input='forest',output="treatment", use="attr", attrcolumn=treatment_field)

        run_command("v.to.rast", input='forest',output="yield_surface", use="attr", attrcolumn=area_field)

        #g.region vect=forest@PERMANENT res=5



        #r.green processing

        run_command("r.green.biomass.legal",energy_tops_hf=energy_tops_hf,energy_cormometric_vol_hf=energy_cormometric_vol_hf,
                    yield_='yield',yield_surface="yield_surface",management="management",treatment="treatment",
                    output_prefix=prefix_output)

        maptot=prefix_output+"_l_bioenergy"
        maphf=prefix_output+"_l_bioenergyHF"
        mapc=prefix_output+"_l_bioenergyC"

        #export map

        run_command("r.out.arc",input=maptot,output=output_directory+"/"+maptot+".asc")
        run_command("r.out.arc",input=maphf,output=output_directory+"/"+maphf+".asc")
        run_command("r.out.arc",input=mapc,output=output_directory+"/"+mapc+".asc")


        #statistic=read_command("r.univar",rast=maptot)


        

        ## add layers to canvas
        rasterptot = output_directory+"/"+maptot+".asc"
        rasterptot_info = QFileInfo(rasterptot)
        base_rasterptot = rasterptot_info.baseName()
        rlayer_ptot = QgsRasterLayer(rasterptot, base_rasterptot)       


        rasterphf = output_directory+"/"+maphf+".asc"
        rasterphf_info = QFileInfo(rasterphf)
        base_rasterphf = rasterphf_info.baseName()
        rlayer_phf = QgsRasterLayer(rasterphf, base_rasterphf) 


        rasterpc = output_directory+"/"+mapc+".asc"
        rasterpc_info = QFileInfo(rasterpc)
        base_rasterpc = rasterpc_info.baseName()
        rlayer_pc = QgsRasterLayer(rasterpc, base_rasterpc) 

        QgsMapLayerRegistry.instance().addMapLayer(rlayer_ptot)  
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_pc) 
        QgsMapLayerRegistry.instance().addMapLayer(rlayer_phf) 


        tempo=time.time() - start_time

        # self.dlg.console_r.appendPlainText("***R.green.biomassfor.theoretical***\n\n")
        # self.dlg.console_r.appendPlainText("Processing executed\n\n")
        # self.dlg.console_r.appendPlainText("Process time: "+str(tempo))
        # self.dlg.console_r.appendPlainText("\n\n")
        # self.dlg.console_r.appendPlainText("Resulted maps:\n")
        # self.dlg.console_r.appendPlainText("1) "+maptot)
        # self.dlg.console_r.appendPlainText("2) "+maphf)
        # self.dlg.console_r.appendPlainText("3) "+mapc)
        # self.dlg.console_r.appendPlainText("\n\n")
        # self.dlg.console_r.appendPlainText("Map added to the table of content\n")
        # self.dlg.console_r.appendPlainText("---------------------------------\n\n")

        #self.dlg.console_r.appendPlainText(statistic)



    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        self.reset_field()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass
